# Premade Layouts

## What is Premade Layouts

Premade Layouts provide premade CSS templates for personalise your [myanimelist.net](https://myanimelist.net/) lists apparence.

## How to use

Even if you don't know what CSS is or how it work, you can just copy the code of your favorite theme and paste it on your list seetings. For more information, you can check [Shishio's Custom Lists & Profiles Club](https://myanimelist.net/clubs.php?cid=19736) on MAL's forum.

